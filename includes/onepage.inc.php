<!-- content -->
<div class="container">
    <div class="col-sm-12 contentdiv">
        <div class="content center counter" id="counter">
            <div class="center col-sm-6 mb1">
                <video width="320" height="240" controls source src="./video/Promo.mp4" type="video/mp4"></video>
                <video width="320" height="240" controls source src="./video/Liemersgroep.mp4" type="video/mp4"> </video>
            </div>
            <div class="center col-sm-6 mb1">
                <video width="320" height="240" controls source src="./video/Video_Minke.mp4" type="video/mp4"> </video>
            </div>
            <div class="col-sm-6 center counter__students">
                <span><strong><?php echo $count ?></strong> <br>Deelnemers</span>
            </div>
            <div class="col-sm-6 center counter__money">
                <span> <strong>€<?php echo number_format($ShowAmount, 2,',','.') ?></strong><br> Opgehaald totaal</span>
            </div>
            <div class="col-sm-6 center counter__money">
                <span> <strong>€<?php echo number_format($ShowAmountAlp, 2,',','.') ?></strong><br> Opgehaald voor Alp d'd’HuZes</span>
            </div>
            <div class="col-sm-6 center counter__money">
                <span> <strong>€<?php echo number_format($ShowAmountSpier, 2,',','.') ?></strong><br> Opgehaald Spieren voor spieren</span>
            </div>
        </div>
        <div class="content left info">
            <div class="col-sm-4">
                <h2 class="title__text">Wat gaan we doen?</h2>
                <p class="textbig">Mavo-leerlingen van het Candea College met gym als examenvak (LO2) lopen en fietsen op 9 juli 2021 voor het goede doel.
                    Leerlingen kiezen zelf of ze rennen of fietsen. Jij kunt ze sponsoren bij deze sportieve prestatie. Al het ingezamelde geld gaat naar het goede doel.
                </p>
            </div>
            <div class="col-sm-4">
                <h2 class="title__text">Welke uitdaging gaan de leerlingen aan?</h2>
                <p class="textbig">DDe leerlingen kiezen voor: 
                    <ul>
                        <li>8 km hardlopen in 50 minuten</li>
                        <li> 40 km fietsen in 2 uur</li>
                    </ul>  
                </p>
            </div>
            <div class="col-sm-4">
                <h2 class="title__text">
                    Waarom doen leerlingen dit?
                </h2>
                <p class="textbig">Dit is een onderdeel van hun lesprogramma. <br>
                    Hierin werken ze aan een aantal vaardigheden: Een trainingsschema maken, volgens het schema trainen, hun conditie verbeteren en hun doel halen.
                </p>
                <p class="textbig">
                    Deze vaardigheden combineren ze met een maatschappelijke opdracht.
                </p>
            </div>
        </div>
        <div class="content col-sm-12 mb1">
            <a class="col-sm-6 content__image jc-center " href="https://www.opgevenisgeenoptie.nl/fundraisers/liemersontour/alpe-dhuzes-2021" target="_blank"><img class="sponsorbig" src="img/alpe.png" alt="spierenvoorspieren"></a>
            <div class="col-sm-6">
                <h2>
                    Alpe d'HuZes
                </h2>
                <p>
                    Een op de drie Nederlanders krijgt kanker. Jaarlijks sterven ruim 44.000 mensen aan kanker. Kankerbestrijding en aandacht voor de kankerpatient zijn heel hard nodig. Alpe d'HuZes is opgericht met als doel de onmacht die ontstaat bij kanker om te zetten in kracht. Onder het motto ‘opgeven is geen optie’ accepteren we geen beperkingen in wat we kunnen en zullen bereiken.
                    100% van de donaties aan het Alpe d’HuZes/KWF-fonds wordt besteed aan wetenschappelijk onderzoek naar kanker en naar de verbetering van kwaliteit van leven van mensen met kanker.
                </p>
                <p>
                    Docente op het Candea College, Fenneke Tacke, kreeg in 2019 de diagnose kanker en is ongeneeslijk ziek. Samen met vrienden en familie zamelt zij geld in voor Alpe d’Huzes.
                </p>
            </div>
        </div>
        <div class="jc-center content col-sm-12 mb1">
            <div class="content">
                <div class="col-sm-6">
                    <h2>
                        Spieren voor spieren
                    </h2>
                    <p>
                        Kinderen moeten onbezorgd kunnen rennen, fietsen, spelen en sporten! Voor zo’n 20.000 kinderen in Nederland is dit niet vanzelfsprekend. Zij hebben een spierziekte. Soms erfelijk, soms niet. Er zijn veel verschillende spierziekten, maar in alle gevallen beperkt het hen in alles wat ze willen en kunnen.
                        Spieren voor Spieren heeft als missie: alle spierziekten bij kinderen verslaan. Daarnaast zet Spieren voor Spieren zich actief in als het gaat om bewustwording creëren en voorlichting geven over spierziekten bij kinderen. 
                    </p>
                </div>
                <a class="content__image jc-center col-sm-6" href="https://www.spierenvoorspieren.nl" target="_blank"><img class="sponsorbig" src="img/svs.png" alt="spierenvoorspieren"></a>
            </div>
        </div>
<!--        <div class="content center col-sm-12">
            <form action="index.php?page=donate" method="get">
                <input type="hidden" value="donate" name="page">
                <input type="submit" class="donate" value="Steun jouw Candea-leerling!">
            </form> -->
        </div>
    </div>
</div>