<?php
$sql = "SELECT * FROM `contestants`";
$Contestants = $PDO->prepare($sql);
$Contestants->execute();
$Contestant = $Contestants->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="col-sm-2">
</div>
<!-- content -->
<div class="container contentdiv" id="vote">
    <div class="content center">
        <?php
        $columns = 3;
        $ct = 0;
        echo "<table class='table'>";
        foreach ($Contestant as $C) {
            $Doels = "SELECT * FROM `DoelenTabel` AS DT WHERE DT.ID = :contdoel_id";
            $Doel = $PDO->prepare($Doels);
            $Doel->execute(
                [':contdoel_id' => $C['doel_id']]
            );
            $ShowDoelen = $Doel->fetchALL(PDO::FETCH_ASSOC);
            $ShowDoel = " ";
            foreach ($ShowDoelen as $DT) {
                $ShowDoel = $DT["Omschrijving"];
            }

            $Amount = "SELECT * FROM `ContestantsDonation` AS CD WHERE CD.Contestants_id = :cont_id";
            $TotalAmount = $PDO->prepare($Amount);
            $TotalAmount->execute([':cont_id' => $C['id'],]);
            $CountAmount = $TotalAmount->fetchAll(PDO::FETCH_ASSOC);

            $ShowAmount = 0;
            foreach ($CountAmount as $TA) {
                $ShowAmount = $ShowAmount + $TA["Amount"];
            }
            if ($ct % $columns == 0) echo "<tr>";
            echo "<td class='tableline'>";
        ?> 
        <div class="col-sm-12">
            <a class="col-sm-4" href="https://www.candea.nl" target="_blank"><img class="sponsorCC" src="img/CClogowit.jpg" alt=""></a>
            <div class=" col-sm-8 textbig left-align"> 

                    <?php
                    echo $C['first_name'];
                    echo ' ';
                    if ($C['infix'] == null) {
                        echo $C['last_name'];
                    } else {
                        echo $C['infix'];
                        echo ' ';
                        echo $C['last_name'];
                    }
                    ?> <br>
                Goed doel:<br><strong><?= $ShowDoel ?></strong> <br>
                Geld opgehaald: €<?= number_format($ShowAmount, 2) ?>
            </div>
            <form action="index.php?page=donateAmount" method="post">
                <input type="hidden" id="id" value=<?php echo $C['id'] ?> name="id">
                <input type="hidden" id="name" value=<?php echo $C['first_name'] ?> name="name">
                <input type="hidden" id="doel" value=<?php echo $C['doel_id'] ?> name="doel">
                <input type="submit" class="donate" value="Steun jouw candea-leerling!">
            </form> 
            <?php
                echo "</td>";
                $ct++;
                if ($ct % $columns == 0) echo "</tr>";
            }
            echo "</table>";
            ?>
        </div>
    </div>
</div>
<div>

    <!-- whitespace -->
    <div class="col-sm-2">
    </div>