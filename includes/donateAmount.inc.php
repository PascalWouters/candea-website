<?php use Mollie\MollieClient;

$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey("test_pa4K6M8wdmDktdVzHvwkJWkQ6g3ghk");
?>

<!-- whitespace -->
<div class="col-sm-2">
</div>
 <!-- content -->
<div class="contentdiv">
     <div class="center" id="donate"> 
            <?php
                $id = $_POST['id'];
                $naam = $_POST['name'];
    //            $doel = [
    //                '1' => 'Alpe d"HuZes',
    //                '2' => 'Spieren voor spieren'
    //            ];
                $doel = $_POST['doel'];
                $payments = [
                    '5.00' => '5 euro',
                    '10.00' => '10 euro',
                    '15.00' => '15 euro',
                    'Vrij' =>'Vrije keuze'
                ];
            ?>
             <?php echo "<br><br><br><br><br><br>" ?>
             <form action="php/donate.php" method="post">
                <input type="hidden" name="id" value="<?= $id ?>">
                <input type="hidden" name="doel" value=" <?= $doel ?>">
                <div class="">Voor welk bedrag wilt u <?php echo $naam ?> sponsoren?</div>
                <?php foreach ($payments as $key => $value): ?>
                        <div class="">
                            <input type="hidden" name="name" value="<?= $naam; ?>">
                            <input type="radio" name="payment" id="payment_<?php echo $key ?>" value="<?php echo $key ?>" />
                            <label for="payment_<?php echo $key ?>"><?php echo $value ?></label>
                        </div>
                <?php endforeach ?>
                <?php echo "<br><br><br>" ?>
                <?php $method = $mollie->methods->get(\Mollie\Api\Types\PaymentMethod::IDEAL, ["include" => "issuers"]); ?>
                    <div class="">Select your bank:
                    </div>
                    <select name="issuer">
                    <?php foreach ($method->issuers() as $issuer) { ?>
                        <option value="<?= htmlspecialchars($issuer->id) ?>"><?= htmlspecialchars($issuer->name) 
                    ?></option>
                    <?php } ?>
                <div class="">
                    <input type="submit" class="donate" value="Doneer!">
                </div>
            </form>
    </div>
</div>
  
