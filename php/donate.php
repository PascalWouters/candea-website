<?php

require_once 'bootstrap.php';
require_once '../connection/dbconnection.php';

use Mollie\MollieClient;

$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey("test_pa4K6M8wdmDktdVzHvwkJWkQ6g3ghk"); // testkey Pascal
//$mollie->setApiKey("test_dsEUv22rykM5wt5PJQcfcWwswUusAE"); testkey Quardraam
//$mollie->setApiKey("live_8tUdK7jJPRbwUB9QBygHKpcJHaugyA"); //produkiekey Quadraam

$_SESSION['id'] = $_POST['id'];
$_SESSION['name'] = $_POST['name'];
$_SESSION['doel'] = $_POST['doel'];
$_SESSION['payment'] = $_POST['payment'];
$_SESSION['bank'] = $_POST['issuer'];

$goals = "SELECT * FROM `DoelenTabel` AS D WHERE D.id = :doel_id";
$goal = $PDO->prepare($goals);
$goal->execute([':doel_id' => $_SESSION['doel'], ]);
$goaldescr = $goal->fetchall(PDO::FETCH_ASSOC);

foreach ($goaldescr as $g) 
{
    $goaldescription = $g['Omschrijving'];  
}

if ($_POST['payment'] == "Vrij") {
    header("Location: ../index.php?page=donateAmountFree");
    die();
}
try
    {

    $payment = $mollie->payments->create([
        "amount" => [
            "currency" => "EUR",
            "value" => number_format($_SESSION['payment'],2),
        ],
        "method" => 'ideal',
        "description" => "Candea college/actie",
        "redirectUrl" => "https://candeasportvoorhetgoededoel.nl/index.php?page=thankspage",
        "webhookUrl" => "https://candeasportvoorhetgoededoel.nl/",
        "issuer" => $_SESSION['bank']
    ]);

    $newAmount = "INSERT INTO ContestantsDonation (Contestants_id, Datum, Doel_id, Amount) VALUES (? ,? ,? ,?)";
    $newAmountInsert = $PDO->prepare($newAmount);
    $newAmountInsert->execute([$_POST['id'], NULL, $_POST['doel'], $_POST['payment']]);

    if (!$payment) {
        die('payment error');
    }

    $url = $payment->getCheckoutUrl();
    header("Location: $url");
    die();

}catch (Exception $e) {
echo "API call failed: " . htmlspecialchars($e->getMessage());
}
