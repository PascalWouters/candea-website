<?php
    require_once 'php/bootstrap.php';
    require "connection/dbconnection.php";

    include "includes/header.inc.php";
    include "includes/whitebar.inc.php";

    if (isset($_GET['page'])) 
    {
      $page = $_GET['page'];
    }
    else 
    {
      $page = "onepage";
      header("Location: index.php?page=onepage");
    }

    if (file_exists('includes/' . $page . '.inc.php')):
      include 'includes/' . $page . '.inc.php';
    else:
      include "includes/404.inc.php";
    endif;

    include "includes/footer.inc.php";

?>