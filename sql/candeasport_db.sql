-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 10 jun 2021 om 15:16
-- Serverversie: 10.4.17-MariaDB
-- PHP-versie: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `candeasport_db`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `contestants`
--

CREATE TABLE `contestants` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `infix` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `contestants`
--

INSERT INTO `contestants` (`id`, `first_name`, `infix`, `last_name`) VALUES
(1, 'Janne', NULL, 'Berendsen'),
(3, 'Pascal', 'Johann', 'Wouters'),
(4, 'Rens', NULL, 'Buunk'),
(5, 'Mark', 'van', 'Egmond'),
(8, 'Linde', 'van', 'Grol'),
(9, 'Bonnie', NULL, 'Harmsen'),
(10, 'Lindsay', NULL, 'Hugen'),
(13, 'Romee', NULL, 'Jansen'),
(14, 'Twan', 'Jansen', ''),
(17, 'Sylvie', NULL, 'Lieve'),
(18, 'Wouter', NULL, 'Loomans'),
(19, 'Chris', NULL, 'Rensen'),
(20, 'Dinand', NULL, 'Teerink'),
(21, 'Milo', NULL, 'Vermeulen'),
(22, 'Feline', NULL, 'Verweij'),
(23, 'Tristan', 'de', 'Vries'),
(24, 'Sem', 'de', 'Wit'),
(25, 'Twan', 'de', 'Bruin'),
(26, 'Zoe', NULL, 'Burgers'),
(27, 'Dila', NULL, 'Dayi'),
(28, 'Rauand', NULL, 'Hassan'),
(29, 'Menno', NULL, 'Hesseling'),
(30, 'Nick', NULL, 'Hoonakker'),
(31, 'Sem', 'de', 'Kruyff'),
(32, 'Daan', NULL, 'Huuskes'),
(33, 'Sjuul', NULL, 'Koster'),
(34, 'Wesley', NULL, 'Nieuwpoort'),
(35, 'Yari', NULL, 'Onck'),
(36, 'Sandra', NULL, 'Salamin'),
(37, 'Eva', NULL, 'Schot'),
(38, 'Jurre', NULL, 'Segers'),
(39, 'Dilayala', NULL, 'Sumen'),
(40, 'Damien', NULL, 'Zweers');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ContestantsDonation`
--

CREATE TABLE `ContestantsDonation` (
  `ID` int(11) NOT NULL,
  `Contestants_id` int(11) NOT NULL,
  `Doel_id` int(11) NOT NULL,
  `Datum` date DEFAULT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `ContestantsDonation`
--

INSERT INTO `ContestantsDonation` (`ID`, `Contestants_id`, `Doel_id`, `Datum`, `Amount`) VALUES
(2, 19, 1, NULL, '10.10');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `DoelenTabel`
--

CREATE TABLE `DoelenTabel` (
  `Id` int(11) NOT NULL,
  `Omschrijving` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `DoelenTabel`
--

INSERT INTO `DoelenTabel` (`Id`, `Omschrijving`) VALUES
(1, 'Alpe d\'Hueze'),
(2, 'Spieren voor spieren');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `contestants`
--
ALTER TABLE `contestants`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_ContestantId` (`Contestants_id`),
  ADD KEY `FK_Doel_Id` (`Doel_id`);

--
-- Indexen voor tabel `DoelenTabel`
--
ALTER TABLE `DoelenTabel`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `contestants`
--
ALTER TABLE `contestants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT voor een tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `DoelenTabel`
--
ALTER TABLE `DoelenTabel`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  ADD CONSTRAINT `FK_ContestantId` FOREIGN KEY (`Contestants_id`) REFERENCES `contestants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Doel_Id` FOREIGN KEY (`Doel_id`) REFERENCES `DoelenTabel` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
