-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 06 jun 2021 om 12:57
-- Serverversie: 10.4.17-MariaDB
-- PHP-versie: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `candeasport_db`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ContestantsDonation`
--

CREATE TABLE `ContestantsDonation` (
  `ID` int(11) NOT NULL,
  `Contestants_id` int(11) NOT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `ContestantsDonation`
--

INSERT INTO `ContestantsDonation` (`ID`, `Contestants_id`, `Amount`) VALUES
(2, 19, '10.10');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_ContestantId` (`Contestants_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `ContestantsDonation`
--
ALTER TABLE `ContestantsDonation`
  ADD CONSTRAINT `FK_ContestantId` FOREIGN KEY (`Contestants_id`) REFERENCES `contestants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
